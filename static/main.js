// ----- custom js ----- //

// hide initial
//$("#searching").hide();
//$("#results-table").hide();
//$("#error").hide();

// global
//var url = 'http://static.pyimagesearch.com.s3-us-west-2.amazonaws.com/vacation-photos/dataset/';
var data_info = {};


$(function() {

  // sanity check
  console.log( "ready!" );

  // image click
  $(".content").click(function() {

    // empty/hide results
    //$("#results").empty();
    //$("#results-table").hide();
    //$("#error").hide();

    // remove active class
    $(".content").removeClass("active")

    // add active class to clicked picture
    $(this).addClass("active")

    if($(".style").hasClass("active")){
      console.log("it got style")
    }else{
      console.log("nope")
    }

    // grab image url
    var image = $(this).attr("src")
    //console.log(image)
    data_info["content"] = image
    console.log(data_info)
    // show searching text
    //$("#searching").show();
    //console.log("searching...")
    $(".user_image").empty()
    $(".user_image").append('<img src='+image+' class="result-img" style="display: block;position: absolute;min-height: 100%; min-width: 100%; ">')


    // ajax request
    if(data_info["style"] != undefined){
      $.ajax({
        type: "POST",
        url: "/transfer",
        data : data_info,
        // handle success
        success: function(result) {
          console.log(result.respone);
          console.log("nice")
          var data = result.respone
          // show table
          //$("#results-table").show();
          // loop through results, append to dom
          //for (i = 0; i < data.length; i++) {
          //  $("#results").append('<tr><th><a href="'+url+data[i]["image"]+'"><img src="'+url+data[i]["image"]+
          //    '" class="result-img"></a></th><th>'+data[i]['score']+'</th></tr>')
          //};
          $(".result").empty()
          $(".result").append('<img src='+data+' class="result-img" style="display: block;position: absolute;min-height: 100%; min-width: 100%; ">')
 
        },
        // handle error
        error: function(error) {
          console.log(error);
          // append to dom
          $("#error").append()
        }
      });
    }

    

  });

  $(".style").click(function() {
    // remove active class
    $(".style").removeClass("active")

    // add active class to clicked picture
    $(this).addClass("active")

    var image = $(this).attr("src")
    //console.log(image)
    data_info["style"] = image.substring(20,24)

    console.log(data_info)

    if(data_info["content"] != undefined){
      $.ajax({
        type: "POST",
        url: "/transfer",
        data : data_info,
        // handle success
        success: function(result) {
          console.log(result.respone);
          var data = result.respone
          // show table
          //$("#results-table").show();
          // loop through results, append to dom
          //for (i = 0; i < data.length; i++) {
          $(".result").empty()
          $(".result").append('<img src='+data+' class="result-img" style="display: block;position: absolute;min-height: 100%; min-width: 100%; ">')
          
        },
        // handle error
        error: function(error) {
          console.log(error);
          // append to dom
          $("#error").append()
        }
      });
    }

  });

  $('#upload-file-btn').click(function(){
    var form_data = new FormData($('#upload-file')[0]);
    $.ajax({
      type: 'POST',
      url: '/upload',
      data: form_data,
      contentType: false,
      cache:false,
      processData:false,
      success: function(data){
        console.log(data.respone)
        $(".user_image").empty()
        $(".user_image").append('<img src='+data.respone+' class="result-img" style="display: block;position: absolute;min-height: 100%; min-width: 100%; ">')
        data_info["content"] = data.respone

        if(data_info["style"] != undefined){
          $.ajax({
            type: "POST",
            url: "/transfer",
            data : data_info,
            // handle success
            success: function(result) {
              console.log(result.respone);
              console.log("nice")
              var data = result.respone
              // show table
              //$("#results-table").show();
              // loop through results, append to dom
              //for (i = 0; i < data.length; i++) {
              //  $("#results").append('<tr><th><a href="'+url+data[i]["image"]+'"><img src="'+url+data[i]["image"]+
              //    '" class="result-img"></a></th><th>'+data[i]['score']+'</th></tr>')
              //};
              $(".result").empty()
              $(".result").append('<img src='+data+' class="result-img" style="display: block;position: absolute;min-height: 100%; min-width: 100%; ">')
     
            },
            // handle error
            error: function(error) {
              console.log(error);
              // append to dom
              $("#error").append()
            }
          });
        }
      }
    })

  })

});