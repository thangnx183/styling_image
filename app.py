from flask import Flask, render_template, request, jsonify, redirect
from PIL import Image
import requests
from StringIO import StringIO
import matplotlib.pyplot as plt
import cv2
import numpy as np 
import os
import glob
from model import style_image
import pyrebase
import time

config = {
    "apiKey": "AIzaSyCbkqSE4sDl9OUbi-fcQBk2JXBw054BVHY",
    "authDomain": "style-app-3fe4c.firebaseapp.com",
    "databaseURL": "https://style-app-3fe4c.firebaseio.com",
    "projectId": "style-app-3fe4c",
    "storageBucket": "style-app-3fe4c.appspot.com",
    "messagingSenderId": "528076779337"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
#user = auth.create_user_with_email_and_password("nxthang1831997@gmail.com","thang75")

user = auth.sign_in_with_email_and_password("nxthang1831997@gmail.com","thang75")
storage = firebase.storage()
#print auth.get_account_info(user["idToken"])


# create flask instance
app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
#INDEX = os.path.join(os.path.dirname(__file__), 'index.csv')

data = {}

# main route
@app.route('/')
def index():
    return render_template("index.html")


@app.route('/transfer', methods=['POST'])
def transfer():

    if request.method == "POST":

        #RESULTS_ARRAY = []

        # get url

        try:
            data["content"] = request.form.get('content')
            data["style"] = request.form.get('style')

            #data["content"] = request.json["content"]
            #data["style"] = request.json["style"]
            print data

            respone = requests.get(data["content"])
            img = Image.open(StringIO(respone.content))
            img = np.array(img)
            img = cv2.resize(img, (256,256))

            pred = style_image.styling(data["style"], img)

            date = int(time.time())
            cv2.imwrite("./styled_image/"+str(date)+".jpeg",pred)

            result = storage.child(str(date)+".jpeg").put("./styled_image/"+str(date)+".jpeg", user["idToken"])
            result_url = storage.child(str(date)+".jpeg").get_url(result["downloadTokens"])
            print result_url
            return jsonify({"respone": result_url}),200

        except:

            # return error
            return jsonify({"sorry": "Sorry, no results! Please try again."}), 500
    
    return jsonify({"nice":"ok"})
        
@app.route('/upload', methods=["POST"])
def upload():
    target = os.path.join(APP_ROOT,'user_images/')
    #print target

    #print request.files.getlist("file")

    old_images = glob.glob('./user_images/*')
    for i in old_images:
        os.remove(i) 

    for file in request.files.getlist("file"):
        #print file 
        file_name = file.filename
        destination = "".join([target,file_name])
        #print destination
        file.save(destination)
        
        date = int(time.time()) 
        #print ("./user_images/" +file_name)
        i = cv2.imread("./user_images/" +file_name)
        #print i.shape
        cv2.imwrite("./user_images/usr_"+str(date)+".jpeg",i)
        img = storage.child("usr_"+str(date)+".jpeg").put("./user_images/usr_"+str(date)+".jpeg", user["idToken"])
        img_url = storage.child("usr_"+str(date)+".jpeg").get_url(img["downloadTokens"])

        print img_url

    return jsonify({"respone":img_url}),200
# run!
if __name__ == '__main__':
    app.jinja_env.cache = {}
    app.run('localhost', debug=True)
